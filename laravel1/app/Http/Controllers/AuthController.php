<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view("page.form");
    }

    public function welcome(Request $request){
        $first = $request["first-name"];
        $last = $request["last-name"];
        return view("page.welcome",["namaDepan"=> $first, "namaBelakang"=>$last]);
    }

    // public function welcome(Request $request){
    //     dd($request->all());
    // }
}
