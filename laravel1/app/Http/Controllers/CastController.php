<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.tambah');
    }

    public function store(Request $request){
        // dd($request->all()); ->semua data terkirim

        //validasi data
        $validated = $request->validate([
            'name' => 'required|min:2',
            'umur' => 'required',
            'bio' => 'required'
        ]); 

        //masukkan data request ke table cast di database
        DB::table('cast')->insert([
            'nama' => $request['name'],
            'umur' => $request['umur'],
            'bio' => $request['bio']  
        ]);

        //lempar ke halaman /cast
        return redirect('/cast');
    }


    public function index(){
        
        $cast = DB::table('cast')->get();
        // dd($cast);
 
        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id){
        $OneCast = DB::table('cast')->find($id);
        return view('cast.detail',['cast'=>$OneCast]);
    }

    public function edit($id){
        $OneCast = DB::table('cast')->find($id);
        return view('cast.edit',['cast'=>$OneCast]); 
    }

    public function update($id, Request $request){
        //validasi data
        $validated = $request->validate([
            'name' => 'required|min:2',
            'umur' => 'required',
            'bio' => 'required'
        ]); 

        //update data
        $affected = DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['name'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
            );
        
        return redirect("/cast");
    }

    public function destroy($id){
        DB::table('cast')->where('id',$id)->delete();

        return redirect("/cast");
     }

}
