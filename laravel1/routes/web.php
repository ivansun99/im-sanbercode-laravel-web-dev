<?php

use Illuminate\Support\Facades\Route;
use  App\Http\Controllers\HomeController;
use  App\Http\Controllers\AuthController;
use  App\Http\Controllers\CastController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,"index"]) ;
Route::get('/register', [AuthController::class,"form"]) ;
Route::post('/welcome', [AuthController::class,"welcome"]) ;
Route::get('/data-tables',function(){
    return view('page.data-table');
});
Route::get('/table',function(){
    return view('page.table');
});

//CRUD Create-Read-Update-Delete

//Create
//route untuk mengarah ke form tambah cast
Route::get('/cast/create',[CastController::class,'create']);
//route untuk menyimpan data inputan ke database
Route::post('/cast',[CastController::class,'store']);


//Read
//route untuk menampilkan semua data di table cast
Route::get("/cast",[CastController::class,"index"]);
//route untuk ambil detail cast berdasarkan id
Route::get("/cast/{id}",[CastController::class,"show"]);

//Update
//Route mengarah ke form edit cast dengan membawa data berdasarkan id
Route::get('/cast/{id}/edit',[CastController::class,'edit']);
//Route update category berdasarkan id
Route::put('/cast/{id}',[CastController::class,'update']);

//Delete
//hapus data berdasarkan id
Route::delete('/cast/{id}',[CastController::class,"destroy"]);