@extends('layout.master')
@section('title')
Buat Account Baru!
@endsection
@section('content')
        
    <form action="/welcome" method="post">
        @csrf
        <fieldset>
            <label>First name:</label><br>
            <input type="text" name="first-name" required><br><br>

            <label>Last name:</label><br>
            <input type="text" name="last-name" required><br><br>

            <label>Gender</label><br>
            <input type="radio" name="gender" required>Male<br>
            <input type="radio" name="gender">Female<br>
            <input type="radio" name="gender">Other<br><br>

            <label>Nationality:</label><br>
            <select name="nationality">
                <option value="indonesia">Indonesia</option>
                <option value="singapore">Singapore</option>
                <option value="malaysia">malaysia</option>
                <option value="australia">Australia</option>
            </select><br><br>

            <label>Language Spoken:</label><br>
            <input type="checkbox" name="language">Bahasa Indoensia<br>
            <input type="checkbox" name="language">English<br>
            <input type="checkbox" name="language">Other<br><br>

            <label>Bio:</label><br><br>
            <textarea name="Bio" rows="10" cols="30"></textarea><br>
        <input type="submit" value="Sign Up">


        </fieldset>



    </form>

@endsection

   