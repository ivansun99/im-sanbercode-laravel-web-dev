@extends('layout.master')
@section('title')
Halaman Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah cast</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Name</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Edit</th>
        <th scope="col">Delete</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
        <tr>
            <td>{{$key + 1}} </td>
            <td>{{$item->nama}} </td>
            <td>{{$item->umur}} </td>
            <td><a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a> </td>
            <td><a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a> </td>
            <td>
                <form action="/cast/{{$item->id}}" method="post">
                    @csrf
                    @method("delete")
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
           
        </tr>
        @empty
        <tr>
            <td>No Cast Found</td>
        </tr>
         
            {{--Tampil kalau databasenya Kosong --}}
        @endforelse
      
      
    </tbody>
  </table>
@endsection



