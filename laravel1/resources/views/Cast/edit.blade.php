@extends('layout.master')
@section('title')
Halaman Edit Cast
@endsection

@section('content')
<form method='post' action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Cast Name</label>
      <input type="text" value="{{$cast->nama}}" name='name' class="form-control" >
    </div>

    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Cast Age</label>
        <input type="number" value="{{$cast->umur}}" name='umur' class="form-control" >
      </div>

      @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
      <label >Cast Bio</label>
      <textarea class="form-control" name='bio' cols='30' rows='10'>{{$cast->bio}}</textarea>
    </div>
    
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
   
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection



