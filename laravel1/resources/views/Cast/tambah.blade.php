@extends('layout.master')
@section('title')
Halaman Create Cast    
@endsection

@section('content')
<form method='post' action="/cast">
    @csrf
    <div class="form-group">
      <label>Cast Name</label>
      <input type="text" name='name' class="form-control" >
    </div>

    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Cast Age</label>
        <input type="number" name='umur' class="form-control" >
      </div>

      @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
      <label >Cast Bio</label>
      <textarea class="form-control" name='bio' cols='30' rows='10'> </textarea>
    </div>
    
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
   
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection



